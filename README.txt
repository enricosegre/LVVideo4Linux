Mostly for fun, I have concocted a way of acquiring video using Labview for Linux,
to mend for the unavailability of IMAQdx on Linux.

The idea is to grab via v4l2 (http://linuxtv.org/), actually through the handler
libv4l2, and to display simply via the picture control.

The package includes wrapper VIs to some public functions of libv4l2.so, high level grabbing oriented VIs, convenient ancillaries and v42l typedefs.
 
A proof of the concept webcam grabber, essentially a translation of the
[Appendix E example to the linuxtv API]
(http://linuxtv.org/downloads/v4l-dvb-apis-old/v4l2grab-example.html),
is also included, as well as some trace of the process used for automatically building the typedefs employed.

Reference: https://lavag.org/files/file/232-lvvideo4linux/

Files are saved for LabVIEW 2014, 32bit. The project was tested on
Ubuntu 14.04 (libv4l-0-1.0.1-1), with LabVIEW 2014 and 2015, 32 and 64bit.
The project originally started on Ubuntu 12 (v4l-utils 0.8.6-1ubuntu2)
and was at the time checked as working down to LabVIEW 2011, so it is quite
possible that it can still be backported.

To run under other platforms (other versions of libv4l, and notably, 64bit
LabVIEW), typedefs have to be regenerated from the header files (libv4l-dev is
required), because some values do depend on the architecture.
Additionally, typedef constants on the block diagrams of various VIs of the
codebase have to be relinked. A script for doing all this automatically is
provided, **GenerateTypedefsAndUpdate.vi**. Run it first, when evaluating
on a new platform. For a step of this process, generate_VIDIOC_values.vi,
a C compiler (gcc) must be present on system.

Discussion can take part on http://lavag.org/topic/17101-video-4-linux/

The development repository of the code is
  https://gitlab.weizmann.ac.il/Segre/LVVideo4Linux

This package is released under GPL 3.0.

=============================================================================
Contents:
---------

README.txt    this file

grab2b.vi     a proof of the concept webcam grabber, essentially a
              translation of the Appendix E example to the linuxtv API,
              http://linuxtv.org/downloads/v4l-dvb-apis/v4l2grab-example.html

grab3-zooming.vi    a variation on the theme, with pixel value inspector
                    and zooming of the picture with scrollwheel

grab2-superpicture_attempt.vi     another variation, failed (too slow) attempt
                                  of implementing an augmented picture xcontrol

allvi.vi    dummy VI including all the subvis and typedefs

HighLevelV4Lgrab/     high level acquisition-oriented subVIs

LV-lib4vl2/    wrappers to the libv4l2.so functions, ancillaries and
                 useful typedefs

wrap_v4l2_mmap_32.so     a tiny wrapper, required to interface LV with
wrap_v4l2_mmap_64.so      v4l2_mmap(), 32 and 64 bit versions

wrap_v4l2_mmap.c      its source, and
compile_wrapper.sh    command to build it

GrabAncillaries/    VIs used by the grab examples, not necessarily part of
                    a "toolkit"

LVVideo4LinuxGrabber.lvproj   project collating the files above

Import_headers/    tools to parse relevant v4l header files and to construct
                   semiautomatically the necessary typedefs

GenerateTypedefsAndUpdate.vi   script to do this construction. To be run
                               before opening the project.

gpl-3.0.txt        license file

=============================================================================
Changelog:
----------

2/9/2013: v0.1.0 first version submitted to 
          http://lavag.org/topic/17101-video-4-linux/

3/9/2013: v0.2.0 High level wrappers in the style "SetVideoFormat",
         "ObtainBuffers", "VideoStreamOn", etc.

20/5/2016: v1.0 codebase organized into orderly project, adapted for both 32bit
           and 64bit. Saved for Labview 2014 32bit.

8/6/2016: v1.0.1 v4l query capabilities implemented, readme file updated for
          submission to LAVA certified CR.

14/6/2016: v1.0.2 low level v4l CLN run in any thread; custom icons and VI
           descriptions added wherever missing.

5/7/2017: v1.0.3 improved picture zoom with mouse well, drag panning

=============================================================================

Enrico Segre, enrico.segre@weizmann.ac.il
