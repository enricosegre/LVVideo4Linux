/* small C program which links with the relevant local headers
   for libv4l, and prints the values of some constants, which
   would be too convolved to derive by parsing a single header file
*/

#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>
#include <libv4l2.h>

int main(int argc, char **argv)
{
    printf("VIDIOC_QUERYCAP	=	%u\n",VIDIOC_QUERYCAP);
    printf("VIDIOC_RESERVED	=	%u\n",VIDIOC_RESERVED);
    printf("VIDIOC_ENUM_FMT	=	%u\n",VIDIOC_ENUM_FMT);
    printf("VIDIOC_G_FMT	=	%u\n",VIDIOC_G_FMT);
    printf("VIDIOC_S_FMT	=	%u\n",VIDIOC_S_FMT);
    printf("VIDIOC_REQBUFS	=	%u\n",VIDIOC_REQBUFS);
    printf("VIDIOC_QUERYBUF	=	%u\n",VIDIOC_QUERYBUF);
    printf("VIDIOC_G_FBUF	=	%u\n",VIDIOC_G_FBUF);
    printf("VIDIOC_S_FBUF	=	%u\n",VIDIOC_S_FBUF);
    printf("VIDIOC_OVERLAY	=	%u\n",VIDIOC_OVERLAY);
    printf("VIDIOC_QBUF	=	%u\n",VIDIOC_QBUF);
    printf("VIDIOC_EXPBUF	=	%u\n",VIDIOC_EXPBUF);
    printf("VIDIOC_DQBUF	=	%u\n",VIDIOC_DQBUF);
    printf("VIDIOC_STREAMON	=	%u\n",VIDIOC_STREAMON);
    printf("VIDIOC_STREAMOFF	=	%u\n",VIDIOC_STREAMOFF);
    printf("VIDIOC_G_PARM	=	%u\n",VIDIOC_G_PARM);
    printf("VIDIOC_S_PARM	=	%u\n",VIDIOC_S_PARM);
    printf("VIDIOC_G_STD	=	%u\n",VIDIOC_G_STD);
    printf("VIDIOC_S_STD	=	%u\n",VIDIOC_S_STD);
    printf("VIDIOC_ENUMSTD	=	%u\n",VIDIOC_ENUMSTD);
    printf("VIDIOC_ENUMINPUT	=	%u\n",VIDIOC_ENUMINPUT);
    printf("VIDIOC_G_CTRL	=	%u\n",VIDIOC_G_CTRL);
    printf("VIDIOC_S_CTRL	=	%u\n",VIDIOC_S_CTRL);
    printf("VIDIOC_G_TUNER	=	%u\n",VIDIOC_G_TUNER);
    printf("VIDIOC_S_TUNER	=	%u\n",VIDIOC_S_TUNER);
    printf("VIDIOC_G_AUDIO	=	%u\n",VIDIOC_G_AUDIO);
    printf("VIDIOC_S_AUDIO	=	%u\n",VIDIOC_S_AUDIO);
    printf("VIDIOC_QUERYCTRL	=	%u\n",VIDIOC_QUERYCTRL);
    printf("VIDIOC_QUERYMENU	=	%u\n",VIDIOC_QUERYMENU);
    printf("VIDIOC_G_INPUT	=	%u\n",VIDIOC_G_INPUT);
    printf("VIDIOC_S_INPUT	=	%u\n",VIDIOC_S_INPUT);
    printf("VIDIOC_G_OUTPUT	=	%u\n",VIDIOC_G_OUTPUT);
    printf("VIDIOC_S_OUTPUT	=	%u\n",VIDIOC_S_OUTPUT);
    printf("VIDIOC_ENUMOUTPUT	=	%u\n",VIDIOC_ENUMOUTPUT);
    printf("VIDIOC_G_AUDOUT	=	%u\n",VIDIOC_G_AUDOUT);
    printf("VIDIOC_S_AUDOUT	=	%u\n",VIDIOC_S_AUDOUT);
    printf("VIDIOC_G_MODULATOR	=	%u\n",VIDIOC_G_MODULATOR);
    printf("VIDIOC_S_MODULATOR	=	%u\n",VIDIOC_S_MODULATOR);
    printf("VIDIOC_G_FREQUENCY	=	%u\n",VIDIOC_G_FREQUENCY);
    printf("VIDIOC_S_FREQUENCY	=	%u\n",VIDIOC_S_FREQUENCY);
    printf("VIDIOC_CROPCAP	=	%u\n",VIDIOC_CROPCAP);
    printf("VIDIOC_G_CROP	=	%u\n",VIDIOC_G_CROP);
    printf("VIDIOC_S_CROP	=	%u\n",VIDIOC_S_CROP);
    printf("VIDIOC_G_JPEGCOMP	=	%u\n",VIDIOC_G_JPEGCOMP);
    printf("VIDIOC_S_JPEGCOMP	=	%u\n",VIDIOC_S_JPEGCOMP);
    printf("VIDIOC_QUERYSTD	=	%u\n",VIDIOC_QUERYSTD);
    printf("VIDIOC_TRY_FMT	=	%u\n",VIDIOC_TRY_FMT);
    printf("VIDIOC_ENUMAUDIO	=	%u\n",VIDIOC_ENUMAUDIO);
    printf("VIDIOC_ENUMAUDOUT	=	%u\n",VIDIOC_ENUMAUDOUT);
    printf("VIDIOC_G_PRIORITY	=	%u\n",VIDIOC_G_PRIORITY);
    printf("VIDIOC_S_PRIORITY	=	%u\n",VIDIOC_S_PRIORITY);
    printf("VIDIOC_G_SLICED_VBI_CAP	=	%u\n",VIDIOC_G_SLICED_VBI_CAP);
    printf("VIDIOC_LOG_STATUS	=	%u\n",VIDIOC_LOG_STATUS);
    printf("VIDIOC_G_EXT_CTRLS	=	%u\n",VIDIOC_G_EXT_CTRLS);
    printf("VIDIOC_S_EXT_CTRLS	=	%u\n",VIDIOC_S_EXT_CTRLS);
    printf("VIDIOC_TRY_EXT_CTRLS	=	%u\n",VIDIOC_TRY_EXT_CTRLS);
    printf("VIDIOC_ENUM_FRAMESIZES	=	%u\n",VIDIOC_ENUM_FRAMESIZES);
    printf("VIDIOC_ENUM_FRAMEINTERVALS	=	%u\n",VIDIOC_ENUM_FRAMEINTERVALS);
    printf("VIDIOC_G_ENC_INDEX	=	%u\n",VIDIOC_G_ENC_INDEX);
    printf("VIDIOC_ENCODER_CMD	=	%u\n",VIDIOC_ENCODER_CMD);
    printf("VIDIOC_TRY_ENCODER_CMD	=	%u\n",VIDIOC_TRY_ENCODER_CMD);
    printf("VIDIOC_S_HW_FREQ_SEEK	=	%u\n",VIDIOC_S_HW_FREQ_SEEK);
    printf("VIDIOC_CREATE_BUFS	=	%u\n",VIDIOC_CREATE_BUFS);
    printf("VIDIOC_PREPARE_BUF	=	%u\n",VIDIOC_PREPARE_BUF);
    printf("VIDIOC_G_SELECTION	=	%u\n",VIDIOC_G_SELECTION);
    printf("VIDIOC_S_SELECTION	=	%u\n",VIDIOC_S_SELECTION);
    printf("VIDIOC_DECODER_CMD	=	%u\n",VIDIOC_DECODER_CMD);
    printf("VIDIOC_TRY_DECODER_CMD	=	%u\n",VIDIOC_TRY_DECODER_CMD);
    printf("VIDIOC_ENUM_DV_TIMINGS	=	%u\n",VIDIOC_ENUM_DV_TIMINGS);
    printf("VIDIOC_QUERY_DV_TIMINGS	=	%u\n",VIDIOC_QUERY_DV_TIMINGS);
    printf("VIDIOC_DV_TIMINGS_CAP	=	%u\n",VIDIOC_DV_TIMINGS_CAP);
    printf("VIDIOC_ENUM_FREQ_BANDS	=	%u\n",VIDIOC_ENUM_FREQ_BANDS);
    printf("VIDIOC_DBG_G_CHIP_INFO	=	%u\n",VIDIOC_DBG_G_CHIP_INFO);
}
