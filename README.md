Mostly for fun, I have concocted a way of acquiring video using Labview for Linux,
to mend for the unavailability of IMAQdx on Linux.

The idea is to grab via v4l2 (http://linuxtv.org/),
actually through the handler libv4l2, and to display simply via the picture control.

The package includes wrapper VIs to some public functions of libv4l2.so, high level grabbing oriented VIs, convenient ancillaries and v42l typedefs.
 
A proof of the concept webcam grabber, essentially a translation of the
[Appendix E example to the linuxtv API](http://linuxtv.org/downloads/v4l-dvb-apis-old/v4l2grab-example.html),
is also included, as well as some trace of the process used for automatically building the typedefs employed.

Reference: https://lavag.org/files/file/232-lvvideo4linux/

Files are saved for LabVIEW 2014, 32bit. The project was tested on
Ubuntu 14.04 (libv4l-0-1.0.1-1), with LabVIEW 2014 and 2015, 32 and 64bit.
The project originally started on Ubuntu 12 (v4l-utils 0.8.6-1ubuntu2)
and was at the time checked as working down to LabVIEW 2011, so it is quite
possible that it can still be backported.

To run under other platforms (other versions of libv4l, and notably, 64bit
LabVIEW), typedefs have to be regenerated from the header files (libv4l-dev is
required), because some values do depend on the architecture.
Additionally, typedef constants on the block diagrams of various VIs of the
codebase have to be relinked. A script for doing all this automatically is
provided, **GenerateTypedefsAndUpdate.vi**. Run it first, when evaluating
on a new platform.
