﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="HighLevelV4Lgrab" Type="Folder">
			<Item Name="ObtainBuffers.vi" Type="VI" URL="../HighLevelV4Lgrab/ObtainBuffers.vi"/>
			<Item Name="QueryCapabilities.vi" Type="VI" URL="../HighLevelV4Lgrab/QueryCapabilities.vi"/>
			<Item Name="RGB8ArrayToFlattenedPicture.vi" Type="VI" URL="../HighLevelV4Lgrab/RGB8ArrayToFlattenedPicture.vi"/>
			<Item Name="SetVideoFormat.vi" Type="VI" URL="../HighLevelV4Lgrab/SetVideoFormat.vi"/>
			<Item Name="VideoStreamOff.vi" Type="VI" URL="../HighLevelV4Lgrab/VideoStreamOff.vi"/>
			<Item Name="VideoStreamOn.vi" Type="VI" URL="../HighLevelV4Lgrab/VideoStreamOn.vi"/>
			<Item Name="Wait4NextBuffer.vi" Type="VI" URL="../HighLevelV4Lgrab/Wait4NextBuffer.vi"/>
		</Item>
		<Item Name="LV-lib4vl2" Type="Folder">
			<Item Name="typedef" Type="Folder">
				<Item Name="mman-common-MAP_.ctl" Type="VI" URL="../LV-libv4l2/typedef/mman-common-MAP_.ctl"/>
				<Item Name="mman-common-PROT_.ctl" Type="VI" URL="../LV-libv4l2/typedef/mman-common-PROT_.ctl"/>
				<Item Name="videodev2-V4L2_BUF_TYPE.ctl" Type="VI" URL="../LV-libv4l2/typedef/videodev2-V4L2_BUF_TYPE.ctl"/>
				<Item Name="videodev2-V4L2_FIELD.ctl" Type="VI" URL="../LV-libv4l2/typedef/videodev2-V4L2_FIELD.ctl"/>
				<Item Name="videodev2-V4L2_MEMORY.ctl" Type="VI" URL="../LV-libv4l2/typedef/videodev2-V4L2_MEMORY.ctl"/>
				<Item Name="videodev2-V4L2_PIX_FMT.ctl" Type="VI" URL="../LV-libv4l2/typedef/videodev2-V4L2_PIX_FMT.ctl"/>
				<Item Name="videodev2_VIDIOC.ctl" Type="VI" URL="../LV-libv4l2/typedef/videodev2_VIDIOC.ctl"/>
				<Item Name="v4l2_buffer cluster.ctl" Type="VI" URL="../LV-libv4l2/typedef/v4l2_buffer cluster.ctl"/>
				<Item Name="fcntl-O_.ctl" Type="VI" URL="../LV-libv4l2/typedef/fcntl-O_.ctl"/>
				<Item Name="errno-base-E.ctl" Type="VI" URL="../LV-libv4l2/typedef/errno-base-E.ctl"/>
				<Item Name="videodev2-VID_TYPE.ctl" Type="VI" URL="../LV-libv4l2/typedef/videodev2-VID_TYPE.ctl"/>
				<Item Name="DeviceCapabilities.ctl" Type="VI" URL="../LV-libv4l2/typedef/DeviceCapabilities.ctl"/>
				<Item Name="v4l2_capability.ctl" Type="VI" URL="../LV-libv4l2/typedef/v4l2_capability.ctl"/>
			</Item>
			<Item Name="Ancillary" Type="Folder">
				<Item Name="v4l2_bufferU32array_to_struct.vi" Type="VI" URL="../LV-libv4l2/Ancillary/v4l2_bufferU32array_to_struct.vi"/>
				<Item Name="struct_to_v4l2_buffer_U32array.vi" Type="VI" URL="../LV-libv4l2/Ancillary/struct_to_v4l2_buffer_U32array.vi"/>
				<Item Name="U32capabilitiesToCluster.vi" Type="VI" URL="../LV-libv4l2/Ancillary/U32capabilitiesToCluster.vi"/>
				<Item Name="libv4l2_path.vi" Type="VI" URL="../LV-libv4l2/Ancillary/libv4l2_path.vi"/>
				<Item Name="U32ArrayToString.vi" Type="VI" URL="../LV-libv4l2/Ancillary/U32ArrayToString.vi"/>
				<Item Name="Error Converter .vi" Type="VI" URL="../LV-libv4l2/Ancillary/Error Converter .vi"/>
			</Item>
			<Item Name="Wrapper" Type="Folder">
				<Item Name="v4l2 ioctl.vi" Type="VI" URL="../LV-libv4l2/Wrapper/v4l2 ioctl.vi"/>
				<Item Name="v4l2 mmap.vi" Type="VI" URL="../LV-libv4l2/Wrapper/v4l2 mmap.vi"/>
				<Item Name="v4l2 open.vi" Type="VI" URL="../LV-libv4l2/Wrapper/v4l2 open.vi"/>
				<Item Name="v4l2 close.vi" Type="VI" URL="../LV-libv4l2/Wrapper/v4l2 close.vi"/>
				<Item Name="v4l2 fd open.vi" Type="VI" URL="../LV-libv4l2/Wrapper/v4l2 fd open.vi"/>
				<Item Name="v4l2 dup.vi" Type="VI" URL="../LV-libv4l2/Wrapper/v4l2 dup.vi"/>
				<Item Name="v4l2 get control.vi" Type="VI" URL="../LV-libv4l2/Wrapper/v4l2 get control.vi"/>
				<Item Name="v4l2 set control.vi" Type="VI" URL="../LV-libv4l2/Wrapper/v4l2 set control.vi"/>
				<Item Name="v4l2 munmap.vi" Type="VI" URL="../LV-libv4l2/Wrapper/v4l2 munmap.vi"/>
			</Item>
		</Item>
		<Item Name="mmap_wrapper" Type="Folder">
			<Item Name="wrap_v4l2_mmap.c" Type="Document" URL="../wrap_v4l2_mmap.c"/>
			<Item Name="compile_wrapper.sh" Type="Document" URL="../compile_wrapper.sh"/>
			<Item Name="wrap_v4l2_mmap_32.so" Type="Document" URL="../wrap_v4l2_mmap_32.so"/>
			<Item Name="wrap_v4l2_mmap_64.so" Type="Document" URL="../wrap_v4l2_mmap_64.so"/>
		</Item>
		<Item Name="GrabAncillaries" Type="Folder">
			<Item Name="MoveToNewOrigin.vi" Type="VI" URL="../GrabAncillaries/MoveToNewOrigin.vi"/>
			<Item Name="PicturePixelInfo.vi" Type="VI" URL="../GrabAncillaries/PicturePixelInfo.vi"/>
			<Item Name="PicturePixelCoordinates.vi" Type="VI" URL="../GrabAncillaries/PicturePixelCoordinates.vi"/>
		</Item>
		<Item Name="Superpicture.xctl" Type="XControl" URL="../Superpicture/Superpicture.xctl"/>
		<Item Name="allvi.vi" Type="VI" URL="../allvi.vi"/>
		<Item Name="grab2b.vi" Type="VI" URL="../grab2b.vi"/>
		<Item Name="grab3-zooming.vi" Type="VI" URL="../grab3-zooming.vi"/>
		<Item Name="grab2-superpicture_attempt.vi" Type="VI" URL="../grab2-superpicture_attempt.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="datatype.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/importsl/GetValueByPointer/datatype.ctl"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
