#include <libv4l2.h>
#include <stdio.h>

/* necessary for Labview since the Call Library Node does not
   admit pointer return types, while v4l2_mmap fails (returns -1)
   if it is no fed to a pointer */

extern int wrap_v4l2_mmap(void *start, size_t length, int prot, int flags,
		int fd, int64_t offset)
{
  unsigned int *bufstart;
  
  //printf("arguments: %d, %x, %d, %d, %d, %x\n",start, length, prot, flags, fd, offset);
  bufstart = v4l2_mmap(start, length, prot, flags, fd, offset);
  //printf("bufstart=%x\n",bufstart);
  return bufstart;
}
