# the warning: "return makes integer from pointer" (twice) is normal here - 
#  in fact this is the hack the wrapper is for

# 32bit version (compiled on a 64 bit system)
# reminder, for gcc -m32 the package required is gcc-multilib
gcc -m32 -c -fpic wrap_v4l2_mmap.c
gcc -m32 -shared -o wrap_v4l2_mmap_32.so wrap_v4l2_mmap.o /usr/lib/i386-linux-gnu/libv4l2.so.0 

# 64bit version
gcc -c -fpic wrap_v4l2_mmap.c
gcc -shared -o wrap_v4l2_mmap_64.so wrap_v4l2_mmap.o /usr/lib/x86_64-linux-gnu/libv4l2.so.0

rm wrap_v4l2_mmap.o

